import facebook

user_token = "EAAMLmPAboywBACzMAoP8dv8QEZAZAM2IDMab30Cmfo9vsa8N0z5CaymiABfNC9d6ZClLO19gML9bIw6v9S9Sy7gZCynL7VMZCSZAqkq046ZCQgz73LHLEuaAl0CUztAKOhIDK8jTL5inNZBmZBTZBgJijg5SwBrlZCkGES72d8LTio8l14ZAqLyLaDWdFNcFFgj143wfJ35399sg8ZCVQnRCS4gb7"
page_id = "101239818139981"


def get_graph():
    graph = facebook.GraphAPI(user_token)
    my_pages = graph.get_object('me/accounts')
    #print (my_pages.items())
    page_access_token = None
    for page in my_pages['data']:
        if page['id'] == page_id:
            page_access_token = page['access_token']
            #print(page)
    graph = facebook.GraphAPI(page_access_token)

    return graph



def create_post(api, msg):
    api.put_object(page_id, "feed", message=msg)


def update_post(api, post_id, msg):
    api.request(post_id + '?message=' + msg, method='POST')


def delete_post(api, post_id):
    api.delete_object(post_id)


def get_latest_post_message(api):
    latest_post = api.request(page_id + '/posts?limit=1')
    return latest_post['data'][0]['message']


def get_latest_post_id(api):
    latest_post = api.request(page_id + '/posts?limit=1')
    return latest_post['data'][0]['id']


def like_post(api, post_id):
    api.put_like(post_id)


def get_likes_count(api, post_id):
    likes = api.request(post_id + '?fields=likes.summary(true)')
    return likes['likes']['summary']['total_count']


def add_post_comment(api, post_id, msg):
    api.put_comment(post_id, msg)


def get_post_comment(api, post_id):
    comment = api.request(post_id + '?fields=comments')
    return comment['comments']['data'][0]['message']
