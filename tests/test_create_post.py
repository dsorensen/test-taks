from core.operations import get_graph, get_latest_post_id,create_post,add_post_comment\
    ,delete_post


post ="hello"
comment_msg = "comment...."

def test_create_post_comment(message):
    api = get_graph()
    create_post(api, message)
    add_post_comment(api, get_latest_post_id(api), comment_msg)
    delete_post(api, get_latest_post_id(api))

