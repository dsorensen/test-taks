from core.operations import get_graph, get_latest_post_id,create_post,delete_post,page_id


post_msg = 'This is a new post!'


def test_delete_post():
    api = get_graph()
    create_post(api, post_msg)
    post_id = get_latest_post_id(api)
    delete_post(api, post_id)
    assert api.request(page_id + '/posts?limit=1')['data'][0]['id'] != post_id, "post was not deleted"
