
from core.operations import get_graph, get_latest_post_id,create_post,\
    get_latest_post_message,delete_post,update_post


post_msg = 'This is a new post!'
updated_msg = 'This is an update to the post!'


def test_update_post():
    api = get_graph()
    create_post(api, post_msg)
    update_post(api, get_latest_post_id(api), updated_msg)
    assert get_latest_post_message(api) == updated_msg, "post was not updated"
    delete_post(api, get_latest_post_id(api))
