from core.operations import get_graph, get_latest_post_id,create_post,delete_post,get_likes_count,like_post


post_msg = 'This is a new post!'

def test_like_post():
    api = get_graph()
    create_post(api, post_msg)
    assert get_likes_count(api, get_latest_post_id(api)) == 0, "post already has a like"
    like_post(api, get_latest_post_id(api))
    assert get_likes_count(api, get_latest_post_id(api)) == 1, "like not added to the post"
    delete_post(api, get_latest_post_id(api))