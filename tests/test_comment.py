
from core.operations import get_graph, get_latest_post_id,create_post,add_post_comment,delete_post,get_post_comment

post_msg = "This is a new post message"
comment_msg = "comment message example"



def test_comment_post():
    api = get_graph()
    create_post(api, post_msg)
    add_post_comment(api, get_latest_post_id(api), comment_msg)
    assert (get_post_comment(api, get_latest_post_id(api))) == comment_msg, "Comment was not added"
    print(comment_msg)
    print(get_post_comment(api, get_latest_post_id(api)))
    delete_post(api, get_latest_post_id(api))

