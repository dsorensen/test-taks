
import facebook
import datetime


token_test_app ="EAAMLmPAboywBAK2yNZAYYDkYvtGZClACVFZBCS9xeKDXTTD3Gse6TZADIZCFvfcknNUv0khYPea4vuNOJ1JJY01HbQ2sPIK6YUC4JhpUiLh52n" \
      "nJ2K9fZA6kVuNDf7V3FiMMdXjUz0i4hJ4GOTkJoPfjZC0aAYrqeLkvVNBXI5dVH3tz4UdTz4qqLfLOhA2VUUaX5y2VZBmOyMnVHRXxeoTPehvKWI" \
      "wjpB9dIkwFVhlowQZDZD"

def __init__(self, config):
    self.pages = config["pages"]
    self.start_date = config["startDate"]
    self.end_date = datetime.today() - timedelta(days=1)  # Only crawl comments older than 24h
    self.base_timeout = 900  # 15 minutes
    self.comment_counter = 0

    # Initialize Facebook Graph API
    if config["facebook"]["userToken"]:
        token = config["facebook"]["userToken"]
    else:
        token = facebook.GraphAPI().get_app_access_token(config["facebook"]["appId"],
                                                         config["facebook"]["appSecret"],
                                                         offline=False)
    self.graph = facebook.GraphAPI(access_token=token_test_app)

